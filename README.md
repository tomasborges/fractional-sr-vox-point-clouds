# Fractional Super-Resolution of Voxelized Point Clouds

This is the repository containing the full range of results from [1]. 
Suffix legend:
- "_up" indicate nearest neighbor interpolation (NNI) upsampling;
- "_ls" indicate NNI upsampling followed by Laplacian smoothing (LS);
- "_lut" indicate super-resolution using the proposed method;
- The final number indicate the scale factor *s* used to get the projection (1.1&le; *s* &le;2).

For example:
- longdress_vox10_1300_ls1 - indicates a projection from longdress_vox10_1300 using NNI+LS as the upsampling method for a scale factor *s*=1.1.
- longdress_vox10_1300_lut10 - indicates a projection from longdress_vox10_1300 using LUT as the upsampling method for a scale factor *s*=2.

## Conditions of use
If you wish to use these results in your research, please cite [1].

For the point cloud datasets, please cite:
### Datasets
1. *longdress* (longdress_vox10_1300), *loot* (loot_vox10_1200), *redandblack* (redandblack_vox10_1550), and *soldier* (soldier_vox10_0690) from [2];
2. *boxer* (boxer_viewdep_vox12), *longdress12* (longdress_viewdep_vox12), *loot12* (loot_viewdep_vox12), *redandblack12* (redandblack_viewdep_vox12), and *thaidancer* (Thaidancer_viewdep_vox12) from [3];
3. *basketball_player* (basketball_player_vox11_00000200), and *dancer* (dancer_vox11_00000001) from [4];
4. *andrew9*, *david9*, *phil2* , *ricardo9*, and *sarah9* from [5];
5. *arco_valentino* (Arco_Valentino_Dense_vox12), *facade_09* (Facade_00009_vox20), *head* (Head_00039), *house* (House_without_roof_00057_vox12), *queen* (queen_frame_0200), and *statue_klimt* (Statue_Klimt_vox12),  from the [MPEG database](http://mpegfs.int-evry.fr/MPEG/PCC/DataSets/pointCloud/CfP/);
6. *biplane* (1x1_Biplane_Combined_000)  from the [JPEG repository](https://jpeg.org/plenodb/);

Additional pre-processing and voxelization of sets 5 and 6 were done as described in [1].

## References
[1] T. M. Borges, "FRACTIONAL SUPER-RESOLUTION OF VOXELIZED POINT CLOUDS", 2021, Dissertação de Mestrado, Departamento de Engenharia Elétrica, Universidade de Brasília, Brasília-DF, p.77. 

[2] E. d’Eon, B. Harrison, T. Myers, and P. A. Chou, “8i Voxelized Full  Bodies,  version 2  –  A  Voxelized  Point  Cloud  Dataset,”input document m40059/M74006, ISO/IEC JTC1/SC29 JointWG11/WG1 (MPEG/JPEG), Geneva, January 2017.

[3] M. Krivokuća, P. A. Chou, and P. Savill, “8i Voxelized Surface Light Field (8iVSLF) Dataset,” ISO/IEC JTC1/SC29 Joint WG11/WG1 (MPEG/JPEG), Ljubljana, input document m42914, July 2018.

[4] Y. Xu, Y. Lu, and Z. Wen, “Owlii Dynamic Human Textured Mesh Sequence Dataset,” ISO/IEC MPEG JTC1/SC29/WG11, Macau, China, m41658, Oct. 2017.

[5] C. Loop, Q. Cai, S. Escolano, and P. Chou, “Microsoft voxelized upper bodies - a voxelized point cloud dataset,” ISO/IEC JTC1/SC29 Joint WG11/WG1 (MPEG/JPEG), input document m38673/M72012, May 2016.

